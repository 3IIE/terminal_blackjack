"""A very simple Blackjack game.
Blackjack game can accept any number of players. Default is four players.
To increase the number of players pass a key value pair:
    Key   : player_count
    Value : 8

        **Note: the player_count must be separated with:
            Windows ->   :        Example: player_count:10
            POSIX   ->   = or :   Example: player_count=6

            Remember there can be no spaces between the key, separator and
            value.

Example with more players:

    python Blackjack.py player_count=7

Or to run with default number of 4 players:

    python Blackjack.py

Note: This game is set up to only use one deck of cards. As such you should
limit the number of players or the deck of cards could run out of cards.

    Flags:

        -h, --help    : Show this message.

        Example:
            python Blackjack --help
"""
from sys import exit as sExit, argv
import random

class Blackjack():
    """ A simple blackjack game. """
    def __init__(self, **kargs:dict):
        """ Set up and play Blackjack. 
You can set the amount of players at the table by passing a dictionary with
the dictionary unpacked using splat EG **:

    Key:    String   ->  "player_count"
    Value:  Integer  ->   8
        **{'player_count': 8}"""

        ## set up the number of players the default is four.
        players = 4
        if 'player_count' in kargs:
            ## the user wants to set the player count so do as the asked
            players = int(kargs['player_count'])

        ## Run a never ending game loop till the user decides their sick of it.
        while True:
            ## give the user an option to use KeyboardInterrupt to exit the
            ## game.
            try:
                ## here we start the game by dealing the players hands from the
                ## deck of cards.
                self.deck, self.hands = deal(shuffle(build_deck()),
                                             players=players)
                ## The user will be player_1. Give them their hand
                self.player = self.hands['player_1']
                ## remove the users hand from the rest of the players hands
                ## so we can automate their decisions easier.
                self.hands.pop('player_1')
                ## we need to show all the players their hands. The user will
                ## not be able to see the first card dealt to each player.
                self.show_hands()
                ## ask the user to preform their game choices. [hit or stay]
                self.user_choices()
                ## go around the table and automate their choices.
                self.round_table()
                print('\n')
                ## We need to show everybody's hands
                self.show_hands(True)
                ## Who won? I hope it was you! lets find out.
                print(self.show_winner())

                ## we need to ask if the user wants to continue playing the
                ## game?
                while True:
                    ask = '\nWould you like to play again?\n[yes or no] > '
                    again = input(ask)
                    again = again.lower()
                    if 1 in [1 for x in again if x in ('n', 'no')]:
                        sExit('\nGoodbye!\n')
                    if 1 in [1 for x in again if x in ('y', 'yes')]:
                        break

            except KeyboardInterrupt:
                #sExit("\nGoodbye!\n") #AttributeError if not passed "e"
                break


    def show_hands(self, reveal:bool=False, player_1:bool=False):
        """ Print player hands to terminal. 
    reveal:   Boolean  -> Shows the tables entire hand revealing the initially
                          hidden cards values.

    player_1: Boolean  -> Excludes showing the entire tables hands and focuses 
                          on showing only the players hand."""

        if not player_1:
            ## we need to print all the players hands at the table
            print("Oponent's hands:")
            ## loop through all the players
            for player in self.hands:
                ## make the players name a little prettier
                who = player.capitalize()
                if reveal:
                    ## it is time to reveal the hidden cards, shorten the long
                    ## variable names so we can keep to 79 characters
                    hand = [str(x) for x in self.hands[player]["hand"]]
                    score = self.hands[player]["score"]
                    ## Create the string that will show player hand.
                    players = f'  {who}: {", ".join(hand).capitalize()}'
                    players += f' | -> {score}'
                    ## print player hand to screen
                    print(players)
                else:
                    ## print player hand to screen hiding their first card
                    print(f'  {who}: x, {self.hands[player]["hand"][1]}')

        ## calculate the user hand
        score = self.calculate(self.player["hand"])
        self.player['score'] = score

        if self.player['score'] != 'Bust' and self.player['score'] > 21:
            ## the user has busted
            self.player['score'] = 'Bust'

        ## print the user's hand to screen
        print(f'\nYour hand: {", ".join(self.player["hand"])} | -> {score}')


    def user_choices(self):
        """Collect user input for the player, 'hit' or 'stay' and do logic to 
see if the player has busted or not."""

        while True:
            ## create string to print player score to screen and ask for
            ## response to, hit or stay?
            hit_stay = f'\nYour card value is {self.player["score"]},\n'
            hit_stay += 'Would you like to hit or stay your hand?\n> '
            try:

                if (self.player['score'] != 'Bust' and
                    self.player['score'] < 21):
                    ## the player has not busted and their score < 21. Ask if
                    ## player wants to hit or stay?
                    answer = input(hit_stay).lower()

                    if 1 in [1 for x in answer if x in ('h', 'hit')]:
                        ## player wants to hit
                        if self.player['score'] >= 22:
                            ## player has busted
                            self.player['score'] = 'Bust'
                            ## show the player their hand
                            self.show_hands(False, True)
                            ## stop the while loop, players hand is over
                            break

                        ## player has not busted so give them another card
                        card = self.deck.pop()
                        ## add card to the players hand
                        self.player['hand'].append(card)
                        ## calculate the players score
                        score = self.calculate(self.player['hand'])
                        ## update the players score
                        self.player['score'] = score
                        ## show the player their hand
                        self.show_hands(False, True)

                    if 1 in [1 for x in answer if x in ('s', 'stay')]:
                        ## player wants to stay their hand so calculate score
                        score = self.calculate(self.player['hand'])
                        if score > 21:
                            ## player has busted
                            self.player['score'] = 'Bust'
                            ## stop the while loop, players hand is over
                            break
                        ## update players score
                        self.player['score'] = score
                        ## stop the while loop, players hand is over
                        break

                else:
                    ## the player has busted. remove this and see if there are issues!
                    self.player['score'] = 'Bust'
                    ## stop the while loop, players hand is over
                    break
            except KeyboardInterrupt:
                break


    def round_table(self):
        """ Run through players and hit or stay based on random choice using if 
cards sum is greater than 16. """

        ## loop through all players hands
        for player, hand in self.hands.items():
            ## get players hand
            hand = hand['hand']
            ## calculate the players hands value
            value = self.calculate(hand)
            ## create loop to add cards to players hand if card <20 and
            ## card > 16.
            while value < random.choice([17, 18, 19]):
                ## select a card from the top of the deck
                card = self.deck.pop()
                ## add card to the player's hand
                hand.append(card)
                ## Calculate the value of the players hand
                value = self.calculate(hand)

            if value > 21:
                ## player has busted
                self.hands[player]['score'] = 'Bust'
            else:
                ## player has not busted
                self.hands[player]['score'] = value


    def show_winner(self):
        """Show who won the hand of Blackjack. Returns the winner formatted to
a string and their card score. """

        self.hands.update({'player_1': self.player})

        ## to get rid of pylint's "too many local variables 17/15" complaint
        loc = {}
        ## make a dictionary with only the key and the score
        loc['scores'] = {k: v['score'] for k, v in self.hands.items()}
        ## make a dictionary of only the players who have busted
        loc['bust'] = [k for k, v in loc['scores'].items() if v == 'Bust']
        ## remove the players who have busted
        loc['scores'] = {k: v for k, v in loc['scores'].items() if v != 'Bust'}
        ## order the dictionary using the scores, highest to lowest value
        loc['scores'] = dict(sorted(loc['scores'].items(),
                                    key=lambda item: item[-1]))

        ## reverse the dictionary to highest value to lowest
        # _keys = list(reversed(loc['scores'].keys()))
        # _vals = reversed(loc['scores'].values())
        # loc['scores'] = {_keys[x]: y for x, y in enumerate(_vals)}

        ## add the busts back in the total scores, see if we can remove this code from program!
        for bst in loc['bust']:
            loc['scores'].update({bst: 'Bust'})

        tie = {} ## we need a holder for the ties

        ## loop to create a dictionary of scores as keys and list of players
        ## as values to figure out ties.
        for key_1, val_1 in loc['scores'].items():
            for key_2, val_2 in loc['scores'].items():
                if key_1 != key_2 and val_1 == val_2:
                    if not val_1 in tie:
                        tie.update({val_1: set({key_1, key_2})})
                    else:
                        tie[val_1].update({key_1, key_2})

        ## get the overall winner from highest score
        winner = list(loc['scores'].keys())[0]

        ## find winner or tie, return a string
        for _key, _val in tie.items():
            if winner in _val:
                ## There was a tie and we need to share :P capitalize winners
                winner = [x.capitalize() for x in _val]
                ## calculate the winners score
                loc['score'] = self.hands[winner[0].lower()]['score']
                ## create award string
                award = f"\n{', '.join(winner)}"
                award += f" have tied with a score of {loc['score']}."
                ## return reward string
                return award

            ## there was a clear winner ;) calculate the winners score
            loc['score'] = self.hands[winner]['score']
            ## create award string
            award = f"\n{winner.capitalize()} won with a score of"
            award += f" {loc['score']}."
            ## return reward string
            return award


    def calculate(self, cards:list):
        """ Calculate the players hand's numeric value. """
        # creates a new list; necessary as without using list(cards) the
        # original list will be updated resulting in loosing card suit and
        # high card information to their integer values. This was unexpected!
        values = list(cards)

        ## loop through cards and get their translated value
        for i, value in enumerate(values):
            values[i] = self.translate(value)

        ## check for aces so we can give them their proper value depending on
        ## if the players score > 21. first set up variable to hold the total.
        total = 0
        if 11 in values:
            while 11 in values:
                if len(values) > 2:
                    ## there is more than 2 values and one of them is an ace
                    ## get total value so we can see if the values sum are > 21
                    total = sum(values)
                    if total > 21:
                        ## the total is > 21 so we will reduce one ace to the
                        ## value of 1
                        total = sum(values) - 10
                        ## remove the aces value from values
                        values.pop(values.index(11))
                        ## replace the aces value in values with 1
                        values.append(1)
                    else:
                        ## the total was less than 21 so we don't have to do
                        ## anything
                        break
                else:
                    ## there are only 2 cards so it is not possible for their
                    ## sum to be greater than 21. set the total to sum of the
                    ## values
                    total = sum(values)
                    break
            ## return the sum of the values
            return total
        ## there were no aces in the hand so we can safely return the sum of
        ## the values.
        return sum(values)


    def translate(self, card:str):
        """ Translate cards value to int and return. For jack, queen, king, 
ace, return numeric values. """

        ## check to see if the card's value is a string. Sanity check!
        if isinstance(card, str):

            ## split the card's string to get it's first value.
            value = card.split(' ')[0]

            ## Better to ask forgiveness than permission.
            try:
                ## here we try to convert the cards value into an integer
                value = int(value)
                ## success the cards value was indeed an integer. Return it.
                return value
            ## Beg for forgiveness, the cards value was defiantly not an
            ## integer.
            except ValueError:
                if value == 'Ace':
                    ## ask if the string's value was Ace and return it's value.
                    return 11
                ## We have a face card that is not an Ace. Return it's value.
                return 10
        ## in normal operation of this class this should never be reached.
        ## better to have it and not need it than to need it and not have it.
        return value


def build_deck():
    """Build a deck of cards. Returns a string representation of a deck of 52
standard playing cards."""
    cards = ['2', '3', '4', '5', '6', '7', '8', '9', '10', 'Jack', 'Queen',
             'King', 'Ace']
    suits = ['Hearts', 'Spades', 'Diamonds', 'Clubs']
    deck = []
    for suit in suits:
        for card in cards:
            deck.append(f"{card} of {suit}")
    return deck


def shuffle(deck:list):
    """Shuffle a deck of cards using random.choice() method and returns the
shuffled deck."""
    mix = []
    while len(deck) != 0:
        card = random.choice(deck)
        deck.pop(deck.index(card))
        mix.append(card)
    return mix


def deal(deck:list, players:int = 4, card_count:int = 2):
    """Deal a deck of cards based on card count to amount of players. Returns
tuple values: remainder of deck not dealt and the hands dealt to players."""
    hands = {f'player_{x + 1}': {} for x in range(0, players)}
    while card_count != 0:
        for player in range(1, players + 1):
            if len(deck) > 0:
                card = deck.pop(0)
            else:
                sExit('\nThe deck of cards has run dry!\n')
            if 'hand' not in hands[f'player_{player}']:
                hands[f'player_{player}']['hand'] = []
            hands[f'player_{player}']['hand'].append(card)
        card_count = card_count - 1

    return (deck, hands)


def odds_in_100():
    """return all odd numbers between 1-100"""
    return [x for x in range(1, 100) if x %2 != 0]


def fizBuz():
    """A game of fizBiz"""
    i = 1
    while i <= 100:
        if i %3 == 0 and i %5 == 0:
            print('fizzBuzz')
        elif i %5 == 0:
            print('buzz')
        elif i %3 == 0:
            print('fizz')
        else:
            print(i)
        i = i + 1

if __name__ == '__main__':

    ## set up the arguments passed to the program file
    args = [x for x in argv[1:] for y in (':', '=') if y not in x]

    ## look for the help flag and if present get help
    if 1 in [1 for x in args for y in ('-h', '--help') if y in x]:
        sExit(f'\n{__doc__}\n')

    ## set up the keyword arguments
    karg = {x.split(y, 1)[0]: x.split(y, 1)[1] for x in argv[1:]
            for y in (':', '=') if y in x}

    if len(karg) != 0:
        ## there were keyword arguments. Pass them to the Blackjack class
        ## and let the classes whitelisting sort it out
        Blackjack(**karg)
    else:
        ## no keyword arguments passed so set up Blackjack with default's.
        Blackjack()
