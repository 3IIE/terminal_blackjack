# terminal_blackjack


A very simple Blackjack game.
Blackjack game can accept any number of players. Default is four players.
To increase the number of players pass a key value pair:
    Key   : player_count
    Value : 8

        **Note: the player_count must be separated with:
            Windows ->   :        Example: player_count:10
            POSIX   ->   = or :   Example: player_count=6

            Remember there can be no spaces between the key, separator and
            value.

##Example with more players:

    python blackjack_simple.py player_count=7

Or to run with default number of 4 players:

    python blackjack_simple.py

Note: This game is set up to only use one deck of cards. As such you should
limit the number of players or the deck of cards could run out of cards.

## Flags:

        -h, --help    : Show this message.

        Example:
            python blackjack_simple --help
